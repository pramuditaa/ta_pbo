import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Instant;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.sql.ResultSet;

public class Parking {
    private JPanel Zky;
    private JTextField tfDate;
    private JLabel lDate;
    private JLabel lBanner;
    private JLabel lNumb;
    private JLabel lType;
    private JTextField tfNumb;
    private JTextField tfFee;
    private JButton btEnterTime;
    private JButton btEdit;
    private JButton btDelete;
    private JButton btPrint;

    private JComboBox<String> cbType;
    private JLabel lRp;
    private JButton btExitTime;
    private JLabel lFee;
    private JTextField tfTime;
    private JTable table1;
    private JPanel buttons;


    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://127.0.0.1/parkingsystem";
    static final String USER = "root";
    static final String PASS = " ";
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    static PreparedStatement pst;

    public Parking() //constructor
    {
        System.out.println("application starting...");
        connect(); //connecting to database

    }

    public static void main (String[] args) {
        JFrame frame = new JFrame("Parking System");
        frame.setContentPane(new Parking().Zky);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    public void connect()
    {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/parkingsystem", "root", " ");
            System.out.println("CONNECTION SUCCESS");

        }
        catch(ClassNotFoundException | SQLException exception){
            System.out.println(exception);
        }
    }
}
